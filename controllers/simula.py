"""Testes Cliente de Simulador Automatos
 UC: Desenvolvimento de Software 2015 01
 SimProgramming G3 - Projeto MGM
 Aluno: 1002599 - Octavio Augusto da Silva Oliveira"""

import platform 
import os
import random
from dialgo_simulator import question_automaton, set_config_automato 
from subprocess import call


def set_simulador():
    if platform.system() == "Windows":
        dir = os.getcwd()+"\\applications\mgm\simulador"
        call("start cmd /K " + "python server.py " + request.vars.port, cwd=dir, shell=True)
    else:
        dir = os.getcwd()+"/applications/mgm/simulador"
        # falta verificar esta chamada
        call("python server.py " + request.vars.port, cwd=dir, shell=True)


def menu_teste():
    response.flash=T("Bem vindo ao MGM!")
    return dict(message=T(""))


def configurar_automato():
    """Pode ser de 1 a 9"""
    str_num_automato = request.vars.num_automato
    num_automato = int(str_num_automato)
    """Pode ser de 1 a 3"""
    str_tipo = request.vars.tipo
    tipo_automato = int(str_tipo)
    data = set_config_automato(request.vars.ip,
                               int(request.vars.port),
                               num_automato, tipo_automato,
                               get_settings_automato(1) +
                               request.vars.quantidade +
                               get_settings_automato(2) +
                               request.vars.estados +
                               get_settings_automato(3) +
                               request.vars.alertas)
    if data == "Automato "+str(num_automato)+" configurado":
        erro = 0
    else:
        erro = 1
    return dict(automato_num=str_num_automato, automato_config=str_tipo,
                value_err=erro, msg_erro=get_error_msg(erro))


def ran_val(max):
    return str(random.randint(1, max))


def configurar_automato_todos():
    config1 = 1
    config2 = 0
    config3 = 0
    # Configura os automatos aleatoriamente
    # Apenas o primeiro para ver se existe ligacao
    erro = 0
    data = set_config_automato(request.vars.ip, int(request.vars.port), 1, 1,
                               get_settings_automato(1) + ran_val(50) +
                               get_settings_automato(2) + ran_val(9) +
                               get_settings_automato(3) + ran_val(9))
    if data == "Automato 1 configurado":
        for i in range(2, 4):
            data = set_config_automato(request.vars.ip, int(request.vars.port), i, 1,
                                       get_settings_automato(1) + ran_val(50) +
                                       get_settings_automato(2) + ran_val(9) +
                                       get_settings_automato(3) + ran_val(9))
            if data == "Automato "+str(i)+" configurado":
                config1 = config1 + 1
        for i in range(4, 7):
            data = set_config_automato(request.vars.ip, int(request.vars.port), i, 1,
                                       get_settings_automato(1) + ran_val(50) +
                                       get_settings_automato(2) + ran_val(9) +
                                       get_settings_automato(3) + ran_val(9))
            if data == "Automato "+str(i)+" configurado":
                config2 = config2 + 1
        for i in range(7, 10):
            data = set_config_automato(request.vars.ip, int(request.vars.port), i, 1,
                                       get_settings_automato(1) + ran_val(50) +
                                       get_settings_automato(2) + ran_val(9) +
                                       get_settings_automato(3) + ran_val(9))
            if data == "Automato "+str(i)+" configurado":
                config3 = config3 + 1
    else:
        erro = 1
    return dict(tipo_1=config1, tipo_2=config2, tipo_3=config3,
                value_err=erro, msg_erro=get_error_msg(erro))


def consultar_automato():
    dados_automato = question_automaton()
    str_num_automato = request.vars.num_automato
    num_automato = int(str_num_automato)
    dados_automato.set_question(request.vars.ip, int(request.vars.port), num_automato)
    # Controlo de erros
    # 1 - Nao ligou automato.
    # 2 - Nao inseriu dados na BD.
    # 3 - Automato nao configurado
    erro = 0
    # Verifica se a ligacao ao simulador foi bem sucedida
    if dados_automato.get_check_connect() == "OK":
        # Se o tipo de automato nao esta acima de 1, significa logo que nao esta configurado,
        # os outros casos virifica
        if int(dados_automato.get_automaton_type()) < 1:
            erro = 3  # Automato nao configurado
        else:
            # Inserir dados do simulador na BD e 2 - controlo de erros no caso de falha de escrita na BD
            try:
                inserir_mensagem(dados_automato.get_automaton_number(),
                                 dados_automato.get_automaton_type(),
                                 dados_automato.get_dateini(),
                                 dados_automato.get_timeini(),
                                 dados_automato.get_total_time(),
                                 dados_automato.get_time_operation(),
                                 dados_automato.get_availability(),
                                 dados_automato.get_total_production(),
                                 dados_automato.get_quantity_produced_hour(),
                                 dados_automato.get_state(),
                                 dados_automato.get_alerts())
            except:
                erro = 2  # Nao inseriu registo na BD
    else:
        erro = 1  # Nao conseguiu ligar ao automato
        
    # Saida de valores do automato para view
    return dict(automato_num=str_num_automato,
                automato_config=dados_automato.get_automaton_type(),
                dateini=dados_automato.get_dateini(),
                timeini=dados_automato.get_timeini(),
                tempo_total=dados_automato.get_total_time(),
                tempo_funcionamento=dados_automato.get_time_operation(),
                disponibilidade=dados_automato.get_availability(),
                total_produzido=dados_automato.get_total_production(),
                quantidade_produzida_hora=dados_automato.get_quantity_produced_hour(),
                estado=dados_automato.get_state(),
                alerta=dados_automato.get_alerts(),
                base_dados=get_error_msg(erro),
                value_err=erro,
                msg_erro=get_error_msg(erro))


def display_msg():
    grid = SQLFORM.smartgrid(db.Automato_mensagens)
    return dict(grid=grid)


def display_estado():
    grid = SQLFORM.smartgrid(db.Estados)
    return dict(grid=grid)


def display_alertas():
    grid = SQLFORM.smartgrid(db.Alertas)
    return dict(grid=grid)


def display_automatos():
    grid = SQLFORM.smartgrid(db.Automatos)
    return dict(grid=grid)
