""" Cliente de Simulador Automatos
 UC: Desenvolvimento de Software 2015 01
 SimProgramming G3 - Projeto MGM
 Aluno: 1002599 - Octavio Augusto da Silva Oliveira
 
Classe para ligar e receber dados do simulador de automatos""" 
class Simulador():
    def set_simulador(self, host, porto):
        self.receved = ""
        self.host = host
        self.porto = porto
    
    def connect_automato(self, arg):
        import socket
        import sys
        HOST = self.host 
        PORT = self.porto    
        s = None
        for res in socket.getaddrinfo(HOST, PORT, socket.AF_UNSPEC, socket.SOCK_STREAM):
            af, socktype, proto, canonname, sa = res
            try:
                s = socket.socket(af, socktype, proto)
            except socket.error as msg:
                s = None
                continue
            try:
                s.connect(sa)
            except socket.error as msg:
                s.close()
                s = None
                continue
            break
        if s is None:
            self.receved = "ERRO LIGACAO"
        else:
            s.sendall(arg)
            self.receved = s.recv(1024)
            s.close()
        
    def get_data(self):
        return self.receved 
        
"""Classe auxiliar para dividir a string enviada pelo simulador, em funcao do separador"""        
class Campos():
    def set_structure(self, ini, separator):
        self.ini = ini
        self.separator = separator  
        
    def get_str_position(self, data):
        return data.find(self.separator)
        
    def get_values(self, position, receved):
        ini = self.ini 
        for x in range(0, position):
            end = ini + self.get_str_position(receved[ini:])
            if end < ini:
                result = receved[ini:]
            else:
                result = receved[ini:end]
            ini = end + 1
        return result
        
    def get_position(self, position, receved):
        count = 0
        pos = 0
        data = receved
        while (count < position + 1):
            k = data.find(self.separator) + 1
            pos = pos + k
            data = data[k:]
            count = count + 1
        return pos
        
"""Classe para determinar o tipo de automato"""    
class TiposAutomatos():
    def set_data(self, receved):
        self.receved = receved
        
        """Determina o tipo de automato em funcao do argumento e do numero de carateres. 
        Retorna um inteiro ou zero se nao encontrou o tipo (dentro dos previstos), neste caso o automato considera-se nao configurado.
        No caso de se implementar outros tipos de automatos esta funcao podera ter de ser alterada"""
    def get_tipo_automato(self, arg, count):
        k = self.receved.find(arg) + len(arg) 
        return int(self.receved[k:k+count])
        
        """Estrutura da string que deve ser entregue a classe Automato: numero de automato; tipo; dia e hora inicio; tempo total; 
        tempo funcionamento disponibilidade; total produzido; quantidade produzida hora; estado; alerta
        Esta clase tem definidos 3 tipos de automatos: 1, 2 e 3
        Os automatos tipo 1 enviam uma string identica a esta: Automato5;Tipo1;2016-03-24;02:46:19;720;699;97;699;1;2;2
        Os automatos tipo 2 enviam uma string identica a esta: Automato5;Tipo2;2016-03-24;02:46:19;...;699;97;699;1;2;2 (;... nao envia)
        Os automatos tipo 3 enviam uma string identica a esta: Automato 5;Tipo 3;2016-03-24;02:46:19;...;699;97;...;1;2;2 (;... nao envia)
        Em 2 temos de inserir o campo tempo total
        Em 3 temos de inserir o campo tempo total e o total produzido"""        
    def get_data(self, tipo):
        if tipo == 1:
            """Nao faz qualqer alteracao. A string enviada pelo automato esta conforme a geral"""
            return self.receved
        elif tipo == 2:
            campos = Campos()
            campos.set_structure(36, ";")
            tempo_total = 100 * int(campos.get_values(1, self.receved)) / int(campos.get_values(2, self.receved)) 
            """Inseriri Tempo total"""
            self.receved = self.receved[:36]+str(tempo_total)+";"+self.receved[36:]
            return self.receved
        elif tipo == 3:
            campos = Campos()
            campos.set_structure(36, ";")
            """tempo total = tempo funcionamento / disponibilidade"""
            tempo_total = 100 * int(campos.get_values(1, self.receved)) / int(campos.get_values(2, self.receved))
            """total produzido = tempo funcionamento * quantidade produzida hora"""
            total_produzido = int(campos.get_values(1, self.receved)) * int(campos.get_values(3, self.receved))  
            """Inseriri Tempo total"""
            self.receved = self.receved[:36]+str(tempo_total)+";"+self.receved[36:]
            pos = campos.get_position(6, self.receved) 
            """Inseriri Total produzido"""
            self.receved = self.receved[:pos]+str(total_produzido)+";"+self.receved[pos:]
            return self.receved     
        else: return ""
        
"""A calss TiposAutomatos deve gerar uma string com a estrutura seguinte:
numero de automato; tipo; dia e hora inicio; tempo total; tempo funcionamento
disponibilidade; total produzido; quantidade produzida hora; estado; alerta"""    
class AutomatoGeral():
    def set_data(self, tipo, separator, receved):
        self.receved = receved
        self.ini = 0
        if tipo > 0 and tipo < 4:
            self.ini = 36
        self.separator = separator
        
    def get_str_position(self, data):
        return data.find(self.separator)
    
    def get_num(self):
        return self.receved[8:9]    
    
    def get_tipo(self):
        return self.receved[14:15]
    
    def get_date(self):
        return self.receved[16:26]
    
    def get_time(self):
        return self.receved[27:35]
        
    def get_values(self, position):
        campos = Campos()
        campos.set_structure(self.ini, self.separator)
        return campos.get_values(position, self.receved)


class question_automaton():
    def set_question(self, ip, port, number_automaton):
        data = query_automato(ip, port, number_automaton)
        self.automaton_number = ""
        self.automaton_type = "0"
        self.dateini = ""
        self.timeini = ""
        self.total_time = ""
        self.time_operation = ""
        self.availability = ""
        self.total_production = ""        
        self.quantity_produced_hour = ""
        self.state = ""
        self.alerts = ""
        if data == "ERRO LIGACAO":
            self.check_connect = data
        else: 
            self.check_connect = "OK"
            automato = TiposAutomatos()
            automato.set_data(data)
            tipo_automato = automato.get_tipo_automato("Tipo", 1)
            if tipo_automato > 0: 
                dados_automato = AutomatoGeral()
                dados_automato.set_data(tipo_automato, ";", automato.get_data(tipo_automato))
                self.automaton_number = dados_automato.get_num()
                self.automaton_type = dados_automato.get_tipo() 
                self.dateini = dados_automato.get_date()
                self.timeini = dados_automato.get_time() 
                self.total_time = dados_automato.get_values(1)
                self.time_operation = dados_automato.get_values(2)
                self.availability = dados_automato.get_values(3)
                self.total_production = dados_automato.get_values(4)            
                self.quantity_produced_hour = dados_automato.get_values(5)
                self.state = dados_automato.get_values(6) 
                self.alerts = dados_automato.get_values(7)
            
    def get_check_connect(self):
        return self.check_connect
        
    def get_automaton_number(self):
        return self.automaton_number 
        
    def get_automaton_type(self):
        return self.automaton_type
    
    def get_dateini(self):
        return self.dateini 
            
    def get_timeini(self):
        return self.timeini 
    
    def get_total_time(self):
        return self.total_time
    
    def get_time_operation(self):
        return self.time_operation
        
    def get_availability(self):
        return self.availability
        
    def get_total_production(self):
        return self.total_production

    def get_quantity_produced_hour(self):
        return self.quantity_produced_hour
        
    def get_state(self):
        return self.state
        
    def get_alerts(self):
        return self.alerts
    
"""Funcao para enviar configuracao para o simulador de automatos. 
Tres tipos de configuracoes para 3 tipos diferentes de automatos."""        


def set_config_automato(host, porto, num_automato, tipo, arg):
    """Fazer a ligacao ao simulador"""
    simulador = Simulador()
    simulador.set_simulador(host, porto)
    """Enviar configuracao"""
    """tipo 1 : /automato=6tipo=1quantidade=20estados=5alertas=5"""
    """tipo 2 : /automato=6tipo=2quantidade=20estados=5 (gera no maximo 10 alertas)"""
    """tipo 3 : /automato=6tipo=3quantidade=20 (gera no maximo 10 estados e alertas)"""
    simulador.connect_automato("/automato="+str(num_automato)+"tipo="+str(tipo)+arg)
    return simulador.get_data()

"""Funcao para consultar automato"""


def query_automato(host, porto, num_automato):
    """Fazer a ligacao ao simulador"""
    simulador = Simulador()
    simulador.set_simulador(host, porto)
    """Interrogar o simulador para saber o seu estado"""
    """/automato=6tipo=2quantidade=20estados=5alertas=5"""
    simulador.connect_automato("/automato="+str(num_automato)) 
    return simulador.get_data() 